#!/bin/bash - 
#===============================================================================
#
#          FILE: main.sh
# 
#         USAGE: ./main.sh -m : run script to configure mail server
#                          -r : run script to configure router
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 09/06/15 09:58
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# The following variables are global variables
# and are used by other scripts.
declare pwd=$( pwd )
declare router_dir=${pwd}/router
declare mx_dir=${pwd}/mail_server

declare option_m="-m"
declare option_r="-r"
declare option_s="-s"

declare dhcp=""
declare rip=""
declare hostap=""
declare unbound=""
declare postfix=""
declare dovecot=""
declare routing=""
declare wireless_if=""
declare dhcp_interfaces=""

source ./main.conf
#----------------------------------------------

for f in ./util_func/*.sh; do source $f; done
for f in ./common_setup/*.sh; do source $f; done

source ${router_dir}/main_router_config.sh
source ${mx_dir}/main_mail_server_config.sh

declare option

if [[ $# > 0 ]]; then
  option=$1
else
  echo "Please provide an option: $option_m for mail server; $option_r for router;"
  echo "                          $option_s for deploying specific services."
  exit 1
fi

function check_main_conf {

  declare opt
  opt=$1

  dhcp_interfaces=$( echo $dhcp_interfaces|xargs )
  wireless_if=${wireless_if//[[:blank:]]/}

  if [[ "$opt" == "$option_s" && "$dhcp" == "true" || "$opt" == "$option_r" ]]; then
    if [[ "$dhcp_interfaces" == "" ]]; then
      echo "Please provide interfaces on which dhcp daemon will be listening."
      exit 1
    fi
  fi

  if [[ "$opt" == "$option_s" && "$hostap" == "true" || "$opt" == "$option_r" ]]; then
    if [[ "$wireless_if" == "" ]]; then
      echo "Please provide wireless interface name."
      exit 1
    fi
  fi

}

function main_setup {
  
  declare opt=$1

  if [[ $opt == $option_m ]]; then
    main_mail_server_config $opt
  elif [[ $opt == $option_r ]]; then
    check_main_conf $opt
    main_router_config $opt
  elif [[ $opt == $option_s ]]; then
    check_main_conf $opt
    main_mail_server_config $opt
    main_router_config $opt
  else
    echo "Please provide an option: $option_m for mail server; $option_r for router;"
    echo "                          $option_s for deploying specific services."
    exit 1
  fi
}

main_setup $option
