#!/bin/bash - 
#===============================================================================
#
#          FILE: create_user.sh
# 
#         USAGE: ./create_user.sh 
# 
#   DESCRIPTION: Add a user account 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 28/05/15 11:49
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function create_user {

  declare username
  declare clear_passwd

  username=$1
  clear_passwd=$2

  # Add the user account
  useradd $username

  # Give it a password
  echo $clear_passwd | passwd $username --stdin
}

