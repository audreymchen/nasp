#!/bin/bash - 
#===============================================================================
#
#          FILE: start_stop_service.sh
# 
#         USAGE: ./start_stop_service.sh service_name start_stop
# 
#   DESCRIPTION: start_stop must be either start or stop
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 09/06/15 21:08
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function start_stop_service {
  declare current_status
  declare service_name
  declare service_new_status

  service_name=$1
  service_new_status=$2

  if [[ "$service_new_status" != "start" && "$service_new_status" != "stop" ]]; then
    echo "Action to service must be either start or stop."
    exit 1
  fi

  current_status=$( systemctl is-active $service_name )

  #if [[ "$current_status" == "unknown" ]]; then
  #  echo "$service_name is unknown."
  #  exit 1
  #fi


  if [[ "$service_new_status" == "start" && "$current_status" == "active" ]]; then
    echo "Restart $service_name"
    systemctl restart $service_name
  elif [[ "$service_new_status" == "start" && "$current_status" != "active" ]]; then
    echo "Start $service_name"
    systemctl start $service_name
  elif [[ "$service_new_status" == "stop" && "$current_status" == "active" ]]; then
    echo "Stop $service_name"
    systemctl stop $service_name
  fi

}

