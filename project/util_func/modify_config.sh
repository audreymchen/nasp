#!/bin/bash - 
#===============================================================================
#
#          FILE: modify_config.sh
# 
#         USAGE: ./modify_config.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 25/05/15 15:51
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function modify_config {
  declare key
  declare value
  declare config_file

  key="$1"
  value="$2"
  config_file="$3"

  sed -i -r "s/($key *= *).*/\1$value/" $config_file
}

function uncomment_line {
  declare key
  declare value
  declare config_file

  key=$1
  value=$2
  config_file=$3

  sed -i -r "/^# *${key} *= *${value} */s/^#//" $config_file
}

function comment_line {
  declare key
  declare value
  declare config_file

  key=$1
  value=$2
  config_file=$3
  
  sed -i -r "/^ *${key} *= *${value} */s/^/#/" $config_file
}

function simple_replace {
  declare old=$1
  declare new=$2

  for ((i=3; i <= $#; i++)); do
    sed -i 's/'"$old"'/'"$new"'/g' "${!i}"
  done
}
#modify_config "$1" "$2" "$3"


