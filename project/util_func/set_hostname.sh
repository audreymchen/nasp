#!/bin/bash - 
#===============================================================================
#
#          FILE: set_hostname.sh
# 
#         USAGE: ./set_hostname.sh <hostname>
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 02/06/15 22:43
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function set_hostname {
  hostnamectl set-hostname "$1"
}

