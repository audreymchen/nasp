#!/bin/bash - 
#===============================================================================
#
#          FILE: enable_disable_service.sh
# 
#         USAGE: ./enable_disable_service.sh service_name service_status
# 
#   DESCRIPTION: service_status must be either "enabled" or "disabled"
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 09/06/15 21:08
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function enable_disable_service {
  declare current_status
  declare service_name
  declare service_new_status

  service_name=$1
  service_new_status=$2
  if [[ "$service_new_status" != "enabled" && "$service_new_status" != "disabled" ]]; then
    echo "Service's new status must be either disabled or enabled."
    exit 1
  fi

  current_status=$( systemctl is-enabled $service_name )

  if [[ $? == 1 && "$current_status" != "disabled" ]]; then
    echo "$current_status"
    exit 1
  fi

  if [[ "$service_new_status" == "enabled" && "$current_status" == "disabled" ]]; then
    echo "Enable $service_name"
    systemctl enable $service_name
  elif [[ "$service_new_status" == "disabled" && "$current_status" == "enabled" ]]; then
    echo "Disable $service_name"
    systemctl disable $service_name
  fi

}
