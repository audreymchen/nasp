#!/bin/bash - 
#===============================================================================
#
#          FILE: pkg_setup.sh
# 
#         USAGE: ./pkg_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 21/05/15 14:29
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function pkg_setup {
  
  declare pkg_name
  declare yum_output
  declare -i exit_status=1

  # The first parameter is the package name
  pkg_name=$1

  yum_output=$( yum list installed $pkg_name 2> /dev/null )

  pkg_installed=$?

  #echo -e "The output from the \"yum list installed\" command is \n$yum_output"

  if [[ $pkg_installed == 0 ]] ; then
    echo "$pkg_name was already installed" 
  else
    # Go ahead to install the packages
    echo "Installing $pkg_name ..."
    yum install -y $pkg_name 1> /dev/null
    echo -e "\nCompleted the installation of $pkg_name!\n"
  fi
}

