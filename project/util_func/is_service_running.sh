#!/bin/bash - 
#===============================================================================
#
#          FILE: is_service_running.sh
# 
#         USAGE: ./is_service_running.sh service_name 
# 
#   DESCRIPTION: Check if a service is running
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 09/06/15 21:08
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function is_service_running {
  declare current_status
  declare service_name

  service_name=$1


  current_status=$( systemctl is-active $service_name )

  if [[ "$current_status" == "active" ]]; then
    return 0
  else
    return 1
  fi

}

#is_service_running $1
#echo $?

