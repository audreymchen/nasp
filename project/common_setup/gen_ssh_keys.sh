#!/bin/bash - 
#===============================================================================
#
#          FILE: gen_ssh_keys.sh
# 
#         USAGE: ./gen_ssh_keys.sh
# 
#   DESCRIPTION: Generate keys for ssh
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 15:50
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function gen_ssh_keys {

  # generate keys
  # still need to manually copy the public key over to bitbucket account
  mkdir ~/.ssh
  echo -e 'y\n' | ssh-keygen -N "" -f ~/.ssh/id_rsa

  # make sure the files under .ssh foler have the right permission
  chmod 700 ~/.ssh
  chmod 600 ~/.ssh/*

}
