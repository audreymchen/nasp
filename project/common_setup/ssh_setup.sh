#!/bin/bash - 
#===============================================================================
#
#          FILE: ssh_setup.sh
# 
#         USAGE: ./ssh_setup.sh 
# 
#   DESCRIPTION: Set up ssh so that when ssh from host workstation 
#                there is no need to provide credential
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 15:42
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function ssh_setup {
  # copy over user mchen on host workstation's public key to the router
  cp ./config_files/authorized_keys ~/.ssh/

  # make sure the file's permission is right
  chmod 600 ~/.ssh/*
}

