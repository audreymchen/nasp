#!/bin/bash - 
#===============================================================================
#
#          FILE: disable_network_manager.sh
# 
#         USAGE: ./disable_network_manager.sh
# 
#   DESCRIPTION: configure hostwide networking setting
#                - disable NetworkManager
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 14:56
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function disable_network_manager {

  # Disable NetworkManager
  #systemctl stop NetworkManager.service
  #systemctl disable NetworkManager.service
  start_stop_service NetworkManager.service stop
  enable_disable_service NetworkManager.service disabled

}

