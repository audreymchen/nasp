#!/bin/bash - 
#===============================================================================
#
#          FILE: disable_firewall_daemon.sh
# 
#         USAGE: ./disable_firewall_daemon.sh
# 
#   DESCRIPTION: configure hostwide networking setting
#                - disable firewalld
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 14:56
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function disable_firewall_daemon {

  # Disable firewall daemon
  #systemctl stop firewalld.service
  #systemctl disable firewalld.service
  start_stop_service firewalld.service stop
  enable_disable_service firewalld.service disabled

}

