#!/bin/bash - 
#===============================================================================
#
#          FILE: selinux_setup.sh
# 
#         USAGE: ./selinux_setup.sh 
# 
#   DESCRIPTION: Disable selinux 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen 
#  ORGANIZATION: 
#       CREATED: 07/05/15 13:48
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function selinux_setup {
  # copy a good copy of selinux config file to /etc/selinux/config to disable selinux
  # cp ./config_files/selinux_config /etc/selinux/config
  modify_config SELINUX disabled /etc/selinux/config

  # make sure it has the right permission
  chmod 644 /etc/selinux/config

  # Change it to permissive mode for the current session
  setenforce 0
}


