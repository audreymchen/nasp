#!/bin/bash - 
#===============================================================================
#
#          FILE: bitbucket_ssh_setup.sh
# 
#         USAGE: ./bitbucket_ssh_setup.sh 
# 
#   DESCRIPTION: Configure ssh to bitbucket so that when connect from the router
#                to bitbucket repository no need to provide credential
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 15:50
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function bitbucket_ssh_setup {
  # Note: need to manually copy the public key over to bitbucket account

  # start ssh-agent in .bashrc file
  cp ./config_files/bashrc ~/.bashrc

  # make sure to have the right permission
  chmod 644 ~/.bashrc

  # run .bashrc file to start ssh-agent
  bash ~/.bashrc
}
