#!/bin/bash - 
#===============================================================================
#
#          FILE: main_mail_server_config.sh
# 
#         USAGE: ./main_mail_server_config.sh
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 28/05/15 11:01
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

source ${mx_dir}/net_if_setup.sh
source ${mx_dir}/postfix_setup.sh
source ${mx_dir}/dovecot_setup.sh
source ${mx_dir}/ntp_setup.sh
source ${mx_dir}/user_accounts_setup.sh

function main_mail_server_config {

  declare opt
  opt=$1

  if [[ "$opt" == "$option_m" ]]; then
    selinux_setup
    disable_firewall_daemon
    disable_network_manager
    set_hostname mail.s1.as.nasp
    net_if_setup
  fi

  if [[ "$opt" == "$option_s" && "$postfix" == "true" || "$opt" == "$option_m" ]]; then
    postfix_setup
    pkg_setup telnet
    ntp_setup
    user_accounts_setup
  fi

  if [[ "$opt" == "$option_s" && "$dovecot" == "true" || "$opt" == "$option_m" ]]; then
    dovecot_setup
  fi
}
