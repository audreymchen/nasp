#!/bin/bash - 
#===============================================================================
#
#          FILE: dovecot_setup.sh
# 
#         USAGE: ./dovecot_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 03/06/15 21:23
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function dovecot_setup {
  
  # install dovecot
  pkg_setup dovecot

  # modify dovecot config file
  cp ${mx_dir}/config_files/dovecot/dovecot.conf /etc/dovecot/dovecot.conf
  cp ${mx_dir}/config_files/dovecot/10*.conf /etc/dovecot/conf.d/

  # make sure file permissions are right
  chmod 644 /etc/dovecot/dovecot.conf
  chmod 644 /etc/dovecot/conf.d/10-auth.conf
  chmod 644 /etc/dovecot/conf.d/10-mail.conf
  
  # start and enable dovecot daemon
  #systemctl start dovecot
  #systemctl enable dovecot
  start_stop_service dovecot start
  enable_disable_service dovecot enabled
}

