#!/bin/bash - 
#===============================================================================
#
#          FILE: user_accounts_setup.sh
# 
#         USAGE: ./user_accounts_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 28/05/15 11:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function user_accounts_setup {

  # Create a user account for the purpose of sending/receiving emails
  create_user audrey "P@ssw0rd"
  create_user audrey.chen "P@ssw0rd"

}

