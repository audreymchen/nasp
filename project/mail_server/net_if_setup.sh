#!/bin/bash - 
#===============================================================================
#
#          FILE: net_if_setup.sh
# 
#         USAGE: ./net_if_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 28/05/15 11:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function net_if_setup {

  # Copy ifcfg-enp03 over 
  cp ${mx_dir}/config_files/ifcfg-enp0s3 /etc/sysconfig/network-scripts/

  # make sure the permission is right
  chmod 644 /etc/sysconfig/network-scripts/ifcfg-enp0s3

  # restart network service
  #systemctl restart network.service
  start_stop_service network.service start

}

