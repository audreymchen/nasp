#!/bin/bash - 
#===============================================================================
#
#          FILE: postfix_setup.sh
# 
#         USAGE: ./postfix_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 28/05/15 11:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function postfix_setup {

  declare config_file="/etc/postfix/main.cf"

  # Install postfix if it's not installed yet
  pkg_setup postfix

  # set up postfix config file
  uncomment_line "inet_interfaces" "all" $config_file
  uncomment_line "home_mailbox" "Maildir\/" $config_file
  comment_line "inet_interfaces" "localhost" $config_file
  modify_config "mydestination" "\$myhostname, localhost.\$mydomain, localhost, \$mydomain" $config_file

  # restart postfix service
  #systemctl restart postfix.service
  start_stop_service postfix.service start
  enable_disable_service postfix.service enabled
}

