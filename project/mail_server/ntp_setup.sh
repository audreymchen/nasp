#!/bin/bash - 
#===============================================================================
#
#          FILE: ntp_setup.sh
# 
#         USAGE: ./ntp_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 28/05/15 11:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function ntp_setup {

  # Install NTP
  pkg_setup ntp

  # synchronize system clock
  timedatectl set-ntp yes

}

