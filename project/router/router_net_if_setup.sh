#!/bin/bash - 
#===============================================================================
#
#          FILE: router_net_if_setup.sh
# 
#         USAGE: ./router_net_if_setup.sh 
# 
#   DESCRIPTION: Configure network interfaces 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 15:02
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function router_net_if_setup {
  
  # set up host-wide network configuration
  cp $router_dir/config_files/network.conf /etc/sysconfig/network

  # set up interface enp0s3, enp0s8
  cp $router_dir/config_files/ifcfg-enp0s3 /etc/sysconfig/network-scripts/
  cp $router_dir/config_files/ifcfg-enp0s8 /etc/sysconfig/network-scripts/

  # make sure the permission is 644
  chmod 644 /etc/sysconfig/network-scripts/ifcfg-enp0s3
  chmod 644 /etc/sysconfig/network-scripts/ifcfg-enp0s8
  chmod 644 /etc/sysconfig/network

  # restart network.service
  #systemctl restart network.service
  start_stop_service network.service start
}



