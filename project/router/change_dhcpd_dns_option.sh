#!/bin/bash - 
#===============================================================================
#
#          FILE: change_dhcpd_dns_option.sh
# 
#         USAGE: ./change_dhcpd_dns_option.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 11/06/15 21:04
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function change_dhcpd_dns_option {
  
  declare internal_dns
  declare config_file
  declare str="option domain-name-servers"

  internal_dns=$1
  config_file=$2

  if [[ $internal_dns == 0 ]]; then
    # Comment out the public DNS 
    sed -i -r "/^\s*${str}\s*142\.232.*;/s/^/#/" $config_file
    # Uncomment the private DNS
    sed -i -r "/^\s*#\s*${str}\s*10\.220.*;/s/^\s*#//" $config_file
  else
    # Comment out the private DNS
    sed -i -r "/^\s*${str}\s*10\.220.*;/s/^/#/" $config_file
    # Uncomment the public DNS
    sed -i -r "/^\s*#\s*${str}\s*142\.232.*;/s/^\s*#//" $config_file
  fi
}

#change_dhcpd_dns_option $1 $2
