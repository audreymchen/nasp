#!/bin/bash - 
#===============================================================================
#
#          FILE: change_wireless_if.sh
# 
#         USAGE: ./change_wireless_if.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 02/06/15 20:01
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function change_wireless_if {

  declare default_if=$1
  declare new_if=$2

  mv "$router_dir/config_files/ifcfg-$default_if" "$router_dir/config_files/ifcfg-$new_if"
  
  simple_replace $default_if $new_if $router_dir/config_files/hostapd.conf $router_dir/config_files/ifcfg-$new_if $router_dir/config_files/dhcpd.service


}
