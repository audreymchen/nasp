#!/bin/bash - 
#===============================================================================
#
#          FILE: main_router_config.sh
# 
#         USAGE: ./main_router_config.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: Need to manually insert VBox Guest Additions CD
#                manually add wireless USB dangle to the router VM
#                
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 14/05/15 15:34
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

source $router_dir/enable_ip_forwarding.sh
source $router_dir/router_net_if_setup.sh
source $router_dir/ripd_setup.sh
source $router_dir/change_dhcpd_dns_option.sh
source $router_dir/dhcp_setup.sh

# the order of the two files should not be changed
source $router_dir/change_wireless_if.sh
source $router_dir/hostapd_setup.sh

source $router_dir/unbound_setup.sh

function main_router_config {

  declare opt=$1

  
  if [[ "$opt" == "$option_s" && "$routing" == "true" || "$opt" == "$option_r" ]]; then
    # Disable SELINUX
    selinux_setup

    enable_ip_forwarding
    disable_firewall_daemon
    disable_network_manager

    # Configure network interfaces
    router_net_if_setup
  fi

  if [[ "$opt" == "$option_s" && "$rip" == "true" || "$opt" == "$option_r" ]]; then
    ripd_setup
  fi

  if [[ "$opt" == "$option_s" && "$hostap" == "true" || "$opt" == "$option_r" ]]; then
    hostapd_setup $wireless_if
  fi

  if [[ "$opt" == "$option_s" && "$dhcp" == "true" || "$opt" == "$option_r" ]]; then
    dhcp_setup "$dhcp_interfaces"
  fi

  if [[ "$opt" == "$option_s" && "$unbound" == "true" || "$opt" == "$option_r" ]]; then
    unbound_setup
  fi
}
