#!/bin/bash - 
#===============================================================================
#
#          FILE: enable_ip_forwarding.sh
# 
#         USAGE: ./enable_ip_forwarding.sh
# 
#   DESCRIPTION: configure hostwide networking setting
#                - enable ip forwarding
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 14:56
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function enable_ip_forwarding {
  # Enable ip forwarding permanently
  echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf

  # Enable it for this login session
  sysctl -w net.ipv4.ip_forward=1
}

