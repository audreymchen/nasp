#!/bin/bash - 
#===============================================================================
#
#          FILE: hostapd_setup.sh
# 
#         USAGE: ./hostapd_setup.sh 
# 
#   DESCRIPTION: Set up wireless access point
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 19/05/15 10:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function hostapd_setup {

  #declare wirelessNIC
  #declare VMname

  # The first positional parameter should be the router VM's name
  #VMname=$1

  # The seconc positional parameter should be the name of the wireless NIC
  #wirelessNIC=$2

  # Add the usb wireless dongle to the virtual machine
  #vboxmanage usbfilter add 0 --target "$VMname" --name "$wirelessNIC" --action hold --active yes
  
  declare ip_link_output
  declare default_if="wlp0s6u1"
  declare new_if

  new_if=$1

  # Install hostapd package
  pkg_setup hostapd

  # If there is automatic change to the wirefless interface name
  # Then change the configurations
  if [[ $default_if != $new_if ]]; then
    change_wireless_if $default_if $new_if
  fi

  # Copy over the config file of wireless interface
  cp "$router_dir/config_files/ifcfg-$new_if" /etc/sysconfig/network-scripts/
  
  # Make sure the file permission is right
  chmod 644 "/etc/sysconfig/network-scripts/ifcfg-$new_if"

  # Copy over the config file of hostapd
  cp $router_dir/config_files/hostapd.conf /etc/hostapd/
  
  # Make sure the file permission is right
  chmod 644 /etc/hostapd/hostapd.conf

  # Restart network service so that it reads the wireless interface config file
  #systemctl restart network.service
  start_stop_service network.service start

  # Start and enable hostapd service
  #systemctl start hostapd.service
  #systemctl enable hostapd.service
  start_stop_service hostapd.service start
  enable_disable_service hostapd.service enabled

  # Check if the wireless interface is up or down.
  # Sometimes it's down and have to restart hostapd to bring it up
  
  ip_link_output=$( ip link show $new_if | grep -A 2 -i "state down" )
  if [[ $ip_link_output != "" ]] ; then
    #systemctl restart hostapd.service
    start_stop_service hostapd.service start
  fi


}

