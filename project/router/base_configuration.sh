#!/bin/bash - 
#===============================================================================
#
#          FILE: base_configuration.sh
# 
#         USAGE: ./base_configuration.sh 
# 
#   DESCRIPTION: Configure basic system
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 14:22
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# install base package
echo "Install base package" >&2
yum -y -v group install base

# install git and vim
echo "Install git and vim" >&2
yum -y install git vim

# Install the necessary packages in order to install Virtualbox Guest Additions later
echo "install some necessary packages in preparation for Virtualbox Guest Addtions installation" >&2
yum -y install kernel-devel kernel-headers dkms gcc gcc-c++ bzip bzip2

#Install epel
# Get the epel rpm package and install it.
echo "Download epel package" >&2
curl -O http://mirror.its.dal.ca/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm 

echo "Install epel package" >&2
yum -y install ./epel-release-7-5.noarch.rpm

# resolve dependancies if any
echo "Do yum update" >&2
yum -y update

# reboot the VM
# On my system, if I don't reboot then the VirtualBox Guest Additions cannot be installed successfully
echo "Reboot" >&2
reboot


