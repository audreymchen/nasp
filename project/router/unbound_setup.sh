#!/bin/bash - 
#===============================================================================
#
#          FILE: unbound_setup.sh
# 
#         USAGE: ./unbound_setup.sh 
# 
#   DESCRIPTION: Set up recursive DNS using unbound
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 26/05/15 21:14
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function unbound_setup {

  declare is_dhcpd_running

  # Install unbound package
  pkg_setup unbound

  # Copy the configuration file of unbound to its designated directory
  cp $router_dir/config_files/unbound.conf /etc/unbound/unbound.conf

  # Make sure the permission is correct
  chmod 644 /etc/unbound/unbound.conf

  # Check the syntax of the config. file
  unbound-checkconf

  # Generate unbound_serverkey file and some other files
  unbound-control-setup

  # Set router's host name
  #set_hostname rtr.s1.as.nasp

  # Start and enable the "unbound" daemon
  #systemctl start unbound.service
  #systemctl enable unbound.service
  start_stop_service unbound.service start
  enable_disable_service unbound.service enabled

  #Update the rounter's enp0s3 interface's config file to use 127.0.0.1 as DNS server
  modify_config DNS1 127.0.0.1 /etc/sysconfig/network-scripts/ifcfg-enp0s3

  # Change /etc/resolv.conf file
  echo -e "search s1.as.nasp\nnameserver 127.0.0.1" > /etc/resolv.conf
  # CentOS automatically save a backup of resolv.conf. 
  # Delete it otherwise it will overwrite some values in the current resolv.conf
  if [[ -e /etc/resolv.conf.save ]]; then
    rm -f /etc/resolv.conf.save
  fi

  # Check if dhcpd.conf exits. If it does, then modify it.
  if [[ -e /etc/dhcp/dhcpd.conf ]]; then
    # Use internal DNS server
    change_dhcpd_dns_option 0 /etc/dhcp/dhcpd.conf
  fi

  # Check if dhcpd is running. If it is, then restart it.
  is_service_running dhcpd.service
  is_dhcpd_running=$?

  if [[ $is_dhcpd_running == 0 ]]; then
    start_stop_service dhcpd.service start
  fi

  # Restart network service
  #systemctl restart network.service
  start_stop_service network.service start
}

