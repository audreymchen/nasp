#!/bin/bash - 
#===============================================================================
#
#          FILE: install_vb_guest.sh
# 
#         USAGE: ./install_vb_guest.sh 
# 
#   DESCRIPTION: Install VirtualBox Guest Additions
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Requires VB Guest Additions are in Virtual CD
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 05/05/2015 08:19
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Install Virtualbox Guest Additions
mkdir vboxcd
mount /dev/cdrom ./vboxcd

cd ./vboxcd
sh ./VBoxLinuxAdditions.run



