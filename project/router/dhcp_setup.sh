#!/bin/bash - 
#===============================================================================
#
#          FILE: dhcp_setup.sh
# 
#         USAGE: ./dhcp_setup.sh 
# 
#   DESCRIPTION: Setup dhcp
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 15:28
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function dhcp_setup { 

  declare interfaces
  declare service_file
  declare dhcpd_conf_file
  declare is_unbound_running

  interfaces="$1"
  service_file=$router_dir/config_files/dhcpd.service
  dhcpd_conf_file=$router_dir/config_files/dhcpd.conf

  # install dhcp package
  pkg_setup dhcp

  #-------------------------------------------
  # configure dhcpd
  #-------------------------------------------
  # Check if unbound is running or not. Based on its state, modify dhcpd.conf accordingly
  is_service_running unbound.service
  is_unbound_running=$?

  if [[ $is_unbound_running == 0 ]]; then
    # unbound is active so use internal DNS
    change_dhcpd_dns_option 0 $dhcpd_conf_file
  else
    # unbound is not running so use public DNS
    change_dhcpd_dns_option 1 $dhcpd_conf_file
  fi

  cp $dhcpd_conf_file /etc/dhcp/dhcpd.conf
  #-------------------------------------------
  

  # parse the config files
  dhcpd -t

  # configure dhcpd.service to have the daemon listen on the specified interfaces
  sed -i -r "s/(.*--no-pid *).*/\1$interfaces/" $service_file
  cp $service_file /etc/systemd/system/

  # change the permissions of the config files
  chmod 644 /etc/dhcp/dhcpd.conf
  chmod 644 /etc/systemd/system/dhcpd.service

  # start and enable dhcp daemon
  systemctl --system daemon-reload
  #systemctl start dhcpd.service
  #systemctl enable dhcpd.service
  start_stop_service dhcpd.service start
  enable_disable_service dhcpd.service enabled
}

