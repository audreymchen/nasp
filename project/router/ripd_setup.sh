#!/bin/bash - 
#===============================================================================
#
#          FILE: ripd_setup.sh
# 
#         USAGE: ./ripd_setup.sh 
# 
#   DESCRIPTION: Setup rip
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 07/05/15 15:28
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

function ripd_setup {
  # install quagga package
  pkg_setup quagga

  # configure zebra
  cp $router_dir/config_files/zebra.conf /etc/quagga/zebra.conf

  # configure ripd
  cp $router_dir/config_files/ripd.conf /etc/quagga/ripd.conf

  # parse the config files
  zebra -Cf /etc/quagga/zebra.conf
  ripd -Cf /etc/quagga/ripd.conf

  # change the owners of config files
  chown quagga:quaggavt /etc/quagga/zebra.conf
  chown quagga:quaggavt /etc/quagga/ripd.conf

  # change the permissions of config files
  chmod 644 /etc/quagga/zebra.conf
  chmod 644 /etc/quagga/ripd.conf

  # start and enable zebra daemon
  #systemctl start zebra.service
  start_stop_service zebra.service start
  #systemctl enable zebra.service
  enable_disable_service zebra.service enabled

  # start and enable ripd daemon
  #systemctl start ripd.service
  #systemctl enable ripd.service
  start_stop_service ripd.service start
  enable_disable_service ripd.service enabled
}

