# Git tag: `FINAL_SUB`

- Please take the tagged version.

# Simple Scripting Deliverable

- The scripts are directly under `Scripts` directory.

# Project Deliverable

- Run `project/main.sh` as root user to deploy servers or deploy specific services based on settings of configurationf file `project/main.conf` 

## Prepare router VM and mail server VM before running the scripts
1. Create a VM with an interface that's bridged to VLAN 2200, and name the VM `AS Router Base`
1. Install the latest minimal CentOS
1. Run `project / router / base_configuration.sh`, then the VM will reboot after the script finishes running
1. Manually insert VBox Guest Additions CD Image to the VM
1. Run `project / router / install_vb_guest.sh`, then run `yum update -y` and shut down the VM
1. At this point, clone the VM and name the clone `AS Mail Base`
1. Add another interface to `AS Router Base`, which is attached to Internal network, `asnet`
1. Add USB WLAN Adapter to `USB Device Filters` on `AS Router Base`
   At this point, `AS Router Base` has three network interfaces: `enp0s3`, `enp0s8` and `wlp0s6u1`.
1. On `AS Mail Base`, change the interface from VLAN 2200 Bridged Adapter to `asnet` Internal network. 
   At this point, `AS Mail Base` has only one interface `enp0s3`.

## To deploy a router with rip, hostap, dhcp and unbound services, run `bash main.sh -r`
		
- Prerequisites of running the script:
	- Option `-r` uses the settings of `wireless_if` and `dhcp_interfaces` from configuration file `Scripts/project/main.conf`
				
## To deploy a mail server with postfix and dovecot service, run `bash main.sh -m`

- This will create two mail accounts: `audrey@s1.as.nasp` and `audrey.chen@s1.as.nasp`. Password for both accounts is `P@ssw0rd`.
- Option `-m` doesn't use the settings of configuration file main.conf
	
## To deploy certain services based on settings in configuration file, run `bash main.sh -s`
		
- Option `-s` uses settings of configuration file `main.conf`.
		
## How to set up configuration file, `project/main.conf`
		
- To deploy rip service, set `rip=true`
- To deploy unbound service, set `unbound=true`
- To deploy postfix service, set `postfix=true`
- To deploy dovecot service, set `dovecot=true`
- To deploy hostap service, set `hostap=true`. When `hostap=true`, the value of `wireless_if` must be specified. This is because sometimes the wireless interface name changes. Usually it's `wlp0s6u1`.
- To deploy dhcp service, set `dhcp=true`. When `dhcp=true`, the value of `dhcp_interfaces` must be specified. `dhcp_interfaces` specifies the interfaces on which dhcp daemon listens. Normally dhcp daemon should listen on `enp0s3` and `wlp0s6u1`.
- To set up basic routing, set `routing=true`. When `routing=true`, the following actions will be taken
	- disable `SELINUX`
	- disable `NetworkManager`
	- disable `firewalld`
	- enable ip forwarding
	- configure interface `enp0s3`, which is bridged to `VLAN 2200`
	- configure interface `enp0s8`, which is attached to Internal Network, `asnet`
	- configure `/etc/sysconfig/network` file
	- start and enable `network.service`