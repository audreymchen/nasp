#!/bin/bash - 
#===============================================================================
#
#          FILE: loop_file_test.sh
# 
#         USAGE: ./loop_file_test.sh [file1] [file2] ...
# 
#   DESCRIPTION: Test if all files that passed in exist or not. 
# 
#       OPTIONS: 
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 30/04/15 15:04
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


source ./loop_file_test_sub.sh


test_file $@

