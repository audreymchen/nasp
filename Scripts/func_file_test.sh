#!/bin/bash - 
#===============================================================================
#
#          FILE: func_file_test.sh
# 
#         USAGE: ./func_file_test.sh 
# 
#   DESCRIPTION: Test if a file exists or not. 
# 
#       OPTIONS: 1. a fully qualified file name
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 30/04/15 15:04
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

declare file_name

if [[ $# > 0 ]]; then
  file_name=$1
else
  echo "Please provide a fully qualified file name."
  exit 0
fi

function test_file  {

  if [[ -e "$1" ]]; then
    echo "The filename: $1 exists."
    exit 0
  else
    echo "The filename: $1 can't be found."
    exit 1
  fi
}

test_file $file_name

