#!/bin/bash - 
#===============================================================================
#
#          FILE: loop_file_test_sub.sh
# 
#         USAGE: ./loop_file_test_sub.sh 
# 
#   DESCRIPTION: Test if all files that passed in exist or not. 
# 
#       OPTIONS: 
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Audrey Chen
#  ORGANIZATION: 
#       CREATED: 30/04/15 15:04
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error


function test_file  {

  declare -i arg=1
  declare -i exit_val=0
  declare file_name

  if [[ $# == 0 ]]; then
    echo "Please provide one or more fully qualified files."
  fi

  while (( arg <= $# )); do
    #eval file_name=\$$arg
    file_name=${!arg}

    if [[ -e "$file_name" ]]; then
      echo "The filename: $file_name exists."
    else
      echo "The filename: $file_name can't be found."
      exit_val=1
    fi

    arg=$(( ++arg ))
  done

  exit $exit_val
}


