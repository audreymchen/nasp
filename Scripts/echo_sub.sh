#!/bin/bash - 
#===============================================================================
#
#          FILE: echo_sub.sh
# 
#         USAGE: ./echo_sub.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 30/04/15 14:52
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
if [[ $# > 0 ]]; then
  echo "The 1st argument is $1"
else
  echo "Please input anything as an argument."
fi

exit 0

