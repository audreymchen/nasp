#!/bin/bash - 
#===============================================================================
#
#          FILE: echo_main.sh
# 
#         USAGE: ./echo_main.sh 
# 
#   DESCRIPTION: Include another bash script to try sourcing
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 30/04/15 14:39
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

# pass the 1st argument to another script
source ./echo_sub.sh $1

