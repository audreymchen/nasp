#!/bin/bash - 
#===============================================================================
#
#          FILE: whereis_ck_arg.sh
# 
#         USAGE: ./whereis_ck_arg.sh command 
# 
#   DESCRIPTION: Outputs the location of a command binary and returns an exit code
#                of zero if it is avaible or 127 if it can't be found
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Thomas Lane (tl), tlane@bcit.ca
#  ORGANIZATION: BCIT
#       CREATED: 04/23/2015 12:26
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

#Variable declarations
#todo place variable declarations here
declare locations
declare whereis_output
declare cmd
declare -i cmd_found=1

#Solicit command that should be searched and store it in the varialbe command
#Get the first positional parameter if it exits
#otherwise use read -p statement to get input from user and store it in the variable: cmd
if [[ $# == 0 ]]; then
  read -p "Please input the command that you would like to find out its location: " cmd
else
  cmd=$1
fi

#Store the output of the whereis invocation that searches for the users inputed 
# command in the variable "whereis_output"
#todo variable assigment to output of command - follows form variable=$( command )
whereis_output=$( whereis $cmd 2> /dev/null )

#Using the variable whereis_output process the output to drop the command name
#store the output in locations
locations=$( (echo $whereis_output | cut -d ":" -f 2-) )

if [[ "$locations" == "" ]]; then
  #the locations are empty i.e. command not found, tell the user 
  #and set the exit code
  echo "$cmd is not found."
  cmd_found=127
else
  #the locations contains the path to the command tell the user
  #and set the exit code
  echo "$cmd is found at: $locations"
  cmd_found=0
fi

echo "Exit code is: $cmd_found"
exit $cmd_found

